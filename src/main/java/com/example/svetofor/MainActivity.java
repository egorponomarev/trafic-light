package com.example.svetofor;


import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        ConstraintLayout bg = (ConstraintLayout) findViewById(R.id.background);

        switch (view.getId()) {
            case R.id.button:
                bg.setBackgroundColor(Color.RED);
                break;

            case R.id.button2:
                bg.setBackgroundColor(Color.GREEN);
                break;

            case R.id.button3:
                bg.setBackgroundColor(Color.YELLOW);
                break;
        }
    }
}
